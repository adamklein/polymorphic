# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
PolymorphicFails::Application.config.secret_key_base = 'ca16a9fb6372f82412b25d8c9c8e67f3473f89b8c1d1014cb69605e7ca4f72a239fb25ad6af9f2cc73cb1babe0cb7f22e9335cb87ef48e1d16dcf772665287ce'
