class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :orderable_id
      t.string :orderable_type

      t.timestamps
    end
  end
end
