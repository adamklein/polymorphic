class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :inventory_request_id

      t.timestamps
    end
  end
end
