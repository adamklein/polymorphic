class Plan < ActiveRecord::Base
  has_many :order_items, as: :orderable
end