class InventoryRequest < ActiveRecord::Base
  has_many    :orders
  has_many    :order_items, through: :orders
  has_many    :items, through: :order_items, source: :orderable, source_type: "Item"
  has_many    :plans, through: :order_items, source: :orderable, source_type: "Plan"
end
